enum class Nivel { INICIANTE, INTERMEDIARIO, AVANCADO }

data class Usuario(val nome: String)

data class ConteudoEducacional(val nome: String, val duracao: Int = 60, val nivel: Nivel)

data class Formacao(val nome: String) {

    val inscritos = mutableListOf<Usuario>()
    val conteudos = mutableListOf<ConteudoEducacional>()

    fun matricular(user: Usuario) {
        if (user in inscritos) {
            println("Usuário: ${user.nome} já cadastrado!")
        } else {
            inscritos.add(user)
        }

    }

    fun adicionarConteudo(conteudo: ConteudoEducacional) {
        conteudos.add(conteudo)
    }

    fun removerConteudo(conteudo: ConteudoEducacional) {
        conteudos.remove(conteudo)
    }

    fun listarConteudo() {
        println("Cursos da formação ${nome}")
        for (conteudo in conteudos) {
            println("${conteudo.nome}")
        }
    }

    fun listarInscritos(): List<Usuario> {
        return inscritos
    }

}

fun main() {

    val usuario1 = Usuario("Juca")
    val usuario2 = Usuario("Jacira")

    val conteudo1 = ConteudoEducacional("Introdução à Kotlin", 60, Nivel.INICIANTE)
    val conteudo2 = ConteudoEducacional("Programação Orientada a Objetos em Kotlin", 120, Nivel.INTERMEDIARIO)

    val formacao = Formacao("Formação Kotlin")

    formacao.adicionarConteudo(conteudo1)
    formacao.adicionarConteudo(conteudo2)

    formacao.listarConteudo()

    formacao.matricular(usuario1)
    formacao.matricular(usuario2)
    formacao.matricular(usuario1)

    println("Inscritos na formação ${formacao.nome}: ${formacao.listarInscritos().joinToString { it.nome }}")

    val usuario3 = Usuario("Joaquim")

    formacao.matricular(usuario3)

    println("Inscritos na formação ${formacao.nome} após matrícula do Pedro: ${formacao.listarInscritos().joinToString { it.nome }}")

    formacao.removerConteudo(conteudo1)

    formacao.listarConteudo()
}